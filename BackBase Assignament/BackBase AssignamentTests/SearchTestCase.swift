//
//  SearchTestCase.swift
//  BackBase AssignamentTests
//
//  Created by Cojocaru Ionut Alexandru on 6/22/20.
//  Copyright © 2020 Cojocaru Ionut Alexandru. All rights reserved.
//

import XCTest
@testable import BackBase_Assignament

class SearchTestCase: XCTestCase {
    
    let dataParser = DataParser()
    var dataSource: [(String, [City])] = []
    var searchResult: [City]?
    let citiyToFind = "A Coruna"

    override func setUpWithError() throws {
        dataParser.getCities(completion: { (result) in
            switch result {
            case .success(let cities):
                self.dataSource = Dictionary(grouping: cities,
                                        by: { $0.name.first!.uppercased() }).sorted(by: { $0.0 < $1.0 })
            case . failure(_):
                break
            }
        })
    }

    override func tearDownWithError() throws {
    }

    func testSearchFunctionality() throws {
        // fail with no data
        XCTAssertFalse(dataSource.isEmpty)
        
        //fail with empty search string
        searchResult = search(in: dataSource, forText: "")
        XCTAssertTrue(searchResult == nil)

        // fail with empty result
        searchResult = search(in: dataSource, forText: "jdfnjksahfsa")
        if searchResult != nil {
            assert(searchResult!.isEmpty)
        } else {
            XCTFail()
        }
        
        // succes
        searchResult = search(in: dataSource, forText: "s g")
        if searchResult != nil {
            XCTAssertFalse(searchResult!.isEmpty)
        } else {
            XCTFail()
        }
    }
    
    func testSearchAccuracy() {
        searchResult = search(in: dataSource, forText: citiyToFind)
        
        if searchResult != nil {
            let count = searchResult!.count
            let name = searchResult?.first?.name ?? ""
            
            assert(count == 1)
            assert(name.elementsEqual(citiyToFind))
        } else {
            XCTFail()
        }
    }

    func testPerformanceExample() throws {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

}

//
//  City.swift
//  BackBase Assignament
//
//  Created by Cojocaru Ionut Alexandru on 6/16/20.
//  Copyright © 2020 Cojocaru Ionut Alexandru. All rights reserved.
//

import Foundation

struct City: Codable, Comparable {
    let country: String
    let name: String
    let id: Decimal
    let coord: Coordinates
    
    private enum CodingKeys: String, CodingKey {
        case country
        case name
        case id = "_id"
        case coord
    }
    
    static func < (lhs: City, rhs: City) -> Bool {
        return lhs.name < rhs.name
    }
    
    static func == (lhs: City, rhs: City) -> Bool {
        var isEqual = true
        
        if !lhs.name.elementsEqual(rhs.name) {
            isEqual = false
        }
        
        if !lhs.country.elementsEqual(rhs.country) {
             isEqual = false
         }
        
        if !lhs.coord.isEqualTo(element: rhs.coord) {
             isEqual = false
         }
        return isEqual
    }
}

struct Coordinates: Codable {
    let longitude: Double
    let latitude: Double
    
    private enum CodingKeys: String, CodingKey {
        case longitude = "lon"
        case latitude = "lat"
    }
    
    func isEqualTo(element: Coordinates) -> Bool {
        if self.latitude.isEqual(to: element.latitude) && self.longitude.isEqual(to: element.longitude) {
            return true
        }
        return false
    }
}

//
//  DataSource.swift
//  BackBase Assignament
//
//  Created by Cojocaru Ionut Alexandru on 6/19/20.
//  Copyright © 2020 Cojocaru Ionut Alexandru. All rights reserved.
//

import UIKit

class DataSource: NSObject, UITableViewDataSource {
    
    // MARK: Properties
    var listDisplayType: ListDisplayType = .list
    var citiesSourceList: [City] = []
    var citiesSourceGroupedList: [(String, [City])] = []
    var currentResultArray: [City]?
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if currentResultArray != nil {
            return 1
        } else {
            return listDisplayType == .list ? 1 : citiesSourceGroupedList.count
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if currentResultArray != nil {
            return currentResultArray?.count ?? 0
        } else {
            return listDisplayType == .list ? citiesSourceList.count : citiesSourceGroupedList[section].1.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifiers.cityCell, for: indexPath) as! CityCell
        if currentResultArray != nil {
            cell.city = currentResultArray?[indexPath.item]
        } else {
            cell.city = listDisplayType == .list ? citiesSourceList[indexPath.item] : citiesSourceGroupedList[indexPath.section].1[indexPath.item]
        }

        return cell
    }
}

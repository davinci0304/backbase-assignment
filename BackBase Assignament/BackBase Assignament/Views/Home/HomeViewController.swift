//
//  ViewController.swift
//  BackBase Assignament
//
//  Created by Cojocaru Ionut Alexandru on 6/16/20.
//  Copyright © 2020 Cojocaru Ionut Alexandru. All rights reserved.
//

import UIKit

enum ListDisplayType {
    case list
    case groupedList
}

protocol ReloadDataProtocol: class {
    func didChangeDataSource()
}

class HomeViewController: UIViewController {
    
    // MARK: Outlets
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    // MARK: Properties
    let searchController = UISearchController(searchResultsController: nil)
    private let dataSource = DataSource()
    
    private lazy var viewModel: CityViewModel = {
        let viewModel = CityViewModel(dataSource: dataSource, delegate: self)
        
        return viewModel
    }()

    
    // MARK: Life cycle
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.navigationBar.prefersLargeTitles = true
        navigationItem.title = AppString.citiesViewControllerTitle
        navigationController?.navigationBar.barTintColor = UIColor.lightPurple
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpUI()
        setUpComponents()
        activityIndicator.startAnimating()
    }
    
    // MARK: Functions
    fileprivate func setUpUI() {
        tableView.separatorStyle = .none
        tableView.backgroundColor = UIColor.lightPurple
        tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 20, right: 0)
        
        let button = UIBarButtonItem(title: AppString.tableViewNavBarRightButtonText,
                                     style: .plain, target: self,
                                     action: #selector(toggleListStyle))
        self.navigationItem.rightBarButtonItem = button
        
        searchController.searchResultsUpdater = self
        searchController.obscuresBackgroundDuringPresentation = false
        searchController.searchBar.placeholder = AppString.searchBarText
        navigationItem.searchController = searchController
        definesPresentationContext = true
    }
    
    @objc func toggleListStyle() {
        viewModel.togelStyle()
    }
    
    fileprivate func setUpComponents() {
        tableView.dataSource = dataSource
        tableView.delegate = self
        registerCells()
        viewModel.getData { [weak self] (result) in
            DispatchQueue.main.async {
                switch result {
                case .success :
                    self?.activityIndicator.stopAnimating()
                case .failure(_):
                    self?.showErrorAlert()
                }
            }
        }
    }
    
    fileprivate func registerCells() {
        tableView.register(UINib(nibName: "CityCell", bundle: nil), forCellReuseIdentifier: CellIdentifiers.cityCell)
    }
        
    fileprivate func showErrorAlert() {
        let alertMessage = CustomAlertNudgeMessage(title: AppString.failedToRetrieveCitiesErrorTitle,
                                     message: AppString.failedToRetrieveCitiesErrorMessage,
                                     type: .error)
        CustomAlertNudgeView.show(withMessage: alertMessage)
    }

}

// MARK: TableView Extension
extension HomeViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let controller = MapViewController()
        if dataSource.currentResultArray != nil {
            controller.city = dataSource.currentResultArray?[indexPath.item]
        } else {
            controller.city = dataSource.listDisplayType == .list ? dataSource.citiesSourceList[indexPath.item] :
                dataSource.citiesSourceGroupedList[indexPath.section].1[indexPath.item]

        }
        self.navigationController?.pushViewController(controller, animated: false)
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        //create a custom view with label to act as the section label
        let headerView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: 30))
        let label = UILabel()
        
        headerView.backgroundColor = .lightPurple
        
        headerView.addSubview(label)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.centerYAnchor.constraint(equalTo: headerView.centerYAnchor).isActive = true
        label.leadingAnchor.constraint(equalTo: headerView.leadingAnchor, constant: 20).isActive = true
        
        if dataSource.listDisplayType == .list || dataSource.currentResultArray != nil {
            label.text = ""
        } else {
            label.text = dataSource.citiesSourceGroupedList[section].0
        }
        
        return headerView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if dataSource.listDisplayType == .list || dataSource.currentResultArray != nil {
            return 0
        } else {
            return 25
        }
    }
}

// MARK: Search Extension
extension HomeViewController: UISearchResultsUpdating {
    func updateSearchResults(for searchController: UISearchController) {
        let searchBar = searchController.searchBar
        let searchText = searchBar.text ?? ""
        viewModel.updateSearchValue(for: searchText)
    }
}

extension HomeViewController: ReloadDataProtocol {
    func didChangeDataSource() {
        tableView.reloadData()
    }
}

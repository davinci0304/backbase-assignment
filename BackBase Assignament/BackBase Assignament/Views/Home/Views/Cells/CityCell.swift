//
//  CityCellTableViewCell.swift
//  BackBase Assignament
//
//  Created by Cojocaru Ionut Alexandru on 6/16/20.
//  Copyright © 2020 Cojocaru Ionut Alexandru. All rights reserved.
//

import UIKit

class CityCell: UITableViewCell {
    // MARK: Outlets
    @IBOutlet weak var cardView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var latitudeLabel: UILabel!
    @IBOutlet weak var longitudeLabel: UILabel!
    
    // MARK: Properties
    var city: City? {
        didSet {
            guard let unwrappedCity = city else { return }
            titleLabel.text = "\(unwrappedCity.name), \(unwrappedCity.country)"

            latitudeLabel.attributedText = AppString.latitudeLabel.concatenateBoldString(withRegularString: "\(unwrappedCity.coord.latitude)")
            longitudeLabel.attributedText = AppString.longitudeLabel.concatenateBoldString(withRegularString: "\(unwrappedCity.coord.longitude)")
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        setUpUI()
    }
    
    // MARK: Functions
    fileprivate func setUpUI() {
        self.selectionStyle = .none
        self.backgroundColor = .clear
        cardView.dropShadow()
        cardView.roundCorners()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}

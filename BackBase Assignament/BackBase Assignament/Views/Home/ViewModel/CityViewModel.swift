//
//  CityViewModel.swift
//  BackBase Assignament
//
//  Created by Cojocaru Ionut Alexandru on 6/19/20.
//  Copyright © 2020 Cojocaru Ionut Alexandru. All rights reserved.
//

import Foundation

class CityViewModel: NSObject {
    
    // MARK: Properties
    let dataParser = DataParser()
    weak var dataSource: DataSource?
    weak var delegate: ReloadDataProtocol?
    
    //initialize with dependency injection (DI)
    //this will help us instantiate our viewModel with a DataSource
    init(dataSource: DataSource, delegate: ReloadDataProtocol) {
        self.dataSource = dataSource
        self.delegate = delegate
    }
    
    func togelStyle() {
        dataSource?.listDisplayType = dataSource?.listDisplayType == .list ? .groupedList : .list
        delegate?.didChangeDataSource()
    }
    
    func getData(completition: @escaping (Result<Void, Error>) -> Void) {
        //get the data from cities.json
        DispatchQueue.runInBackground(background: { [weak self]  in
            self?.dataParser.getCities { (result) in
                switch result {
                case .success(let cities):
                    if !cities.isEmpty {
                        //sort the cities alphabetically
                        self?.dataSource?.citiesSourceList = cities.sorted()
                        //group the cities by the first letter then sort them alphabetically by that letter
                        //this representation of data allow as to display it in two different ways for the user to choose his preferences
                        //but also it will help us for a faster search so when te user will type the first letter we can get our tuple based on that later and we will not search the entire dataset
                        //splitting the big set in grouped sets is beneficial for search, reducing the complexity and time consumption
                        self?.dataSource?.citiesSourceGroupedList = Dictionary(grouping: self?.dataSource?.citiesSourceList ?? [],
                                                                              by: { $0.name.first!.uppercased() }).sorted(by: { $0.0 < $1.0 })
                    }
                    completition(.success(()))
                case .failure(let error):
                    completition(.failure(error))
                }
            }
        }) { [weak self] in
            self?.delegate?.didChangeDataSource()
        }
    }
    
    func updateSearchValue(for text: String) {
        DispatchQueue.runInBackground(background: { [weak self] in
            self?.dataSource?.currentResultArray = search(in: self?.dataSource?.citiesSourceGroupedList ?? [], forText: text)
        }) { [weak self] in
            self?.delegate?.didChangeDataSource()
        }
    }
}

//
//  MapViewController.swift
//  BackBase Assignament
//
//  Created by Cojocaru Ionut Alexandru on 6/16/20.
//  Copyright © 2020 Cojocaru Ionut Alexandru. All rights reserved.
//

import UIKit
import MapKit

class MapViewController: UIViewController {
    
    // MARK: Properties
    let mapView = MKMapView()
    
    var city: City? {
         didSet {
            guard let unwrappedCity = city else { return }
            navigationItem.title = unwrappedCity.name
            setMapLocation(for: unwrappedCity)
         }
     }
    
    // MARK: Life cycle
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.navigationBar.prefersLargeTitles = false
        navigationController?.navigationBar.barTintColor = UIColor.white
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpUI()
    }
    
    // MARK: Functions
    fileprivate func setUpUI() {
        self.view.addSubview(mapView)
        mapView.translatesAutoresizingMaskIntoConstraints = false

        mapView.topAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.topAnchor).isActive = true
        mapView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor).isActive = true
        mapView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor).isActive = true
        mapView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor).isActive = true
        
        let button = UIBarButtonItem(title: AppString.mapViewControllerNavBarRightButtonText,
                                     style: .plain, target: self,
                                     action: #selector(toggleMapStyle))
        self.navigationItem.rightBarButtonItem = button
    }
    
    @objc func toggleMapStyle() {
        if mapView.mapType == .standard {
            mapView.mapType = .satellite
        } else {
            mapView.mapType = .standard
        }
    }
    
    fileprivate func setMapLocation(for city: City) {
        let location = CLLocation(latitude: city.coord.latitude, longitude: city.coord.longitude)
        let regionRadius: CLLocationDistance = 1500
        let region = MKCoordinateRegion(center: location.coordinate, latitudinalMeters: regionRadius, longitudinalMeters: regionRadius)
        mapView.setRegion(region, animated: true)
        
        let annotation = MKPointAnnotation()
        annotation.coordinate = CLLocationCoordinate2D(latitude: city.coord.latitude, longitude: city.coord.longitude)
        mapView.addAnnotation(annotation)
    }
}

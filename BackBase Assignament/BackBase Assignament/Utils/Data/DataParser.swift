//
//  DataParser.swift
//  BackBase Assignament
//
//  Created by Cojocaru Ionut Alexandru on 6/16/20.
//  Copyright © 2020 Cojocaru Ionut Alexandru. All rights reserved.
//

import Foundation

class DataParser {
    public func getCities(completion: @escaping (Result<[City], Error>) -> Void) {
        guard let fileLocation = Bundle.main.url(forResource: "cities", withExtension: "json")
            else {
                let error = NSError.fileError
                return completion(.failure(error))
        }
        do {
            let data = try Data(contentsOf: fileLocation)
            let jsonDecoder = JSONDecoder()
            let cities = try jsonDecoder.decode([City].self, from: data)
            
            return completion(.success(cities))
        } catch {
            return completion(.failure(error))
        }
    }
}

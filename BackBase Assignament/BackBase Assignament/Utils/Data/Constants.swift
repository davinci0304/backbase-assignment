//
//  Constants.swift
//  BackBase Assignament
//
//  Created by Cojocaru Ionut Alexandru on 6/18/20.
//  Copyright © 2020 Cojocaru Ionut Alexandru. All rights reserved.
//

import UIKit

struct Constants {
    static let alertNudegeHeight: CGFloat = 70
    static let fastAnimationDuration = 0.4
}

//
//  BinarySearch.swift
//  BackBase Assignament
//
//  Created by Cojocaru Ionut Alexandru on 6/16/20.
//  Copyright © 2020 Cojocaru Ionut Alexandru. All rights reserved.
//

import Foundation

func binarySearch(array: [(String, [City])], value: String) -> [City]  {
    var left = 0
    var right = array.count - 1
    while left <= right {

        let middle = Int(floor(Double(left + right) / 2.0))

        if array[middle].0 < value {
            left = middle + 1
        } else if array[middle].0 > value {
            right = middle - 1
        } else {
            return array[middle].1
        }
    }
    return []
}

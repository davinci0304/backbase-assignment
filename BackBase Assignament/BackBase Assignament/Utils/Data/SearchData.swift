//
//  SearchData.swift
//  BackBase Assignament
//
//  Created by Cojocaru Ionut Alexandru on 6/18/20.
//  Copyright © 2020 Cojocaru Ionut Alexandru. All rights reserved.
//

import Foundation

//input: the gouped array of cities we want to find our value in
//output: a simple array pc cities that we can display in the table view

func search(in array: [(String, [City])], forText text: String) -> [City]? {
    //search cities by the city.name
    //if the text is empty we must get back to the default data
    if text.isEmpty {
        return nil
    } else {
        //get the first letter for search input to select the right array we will further use in our search
        let guidingLetter = text.first?.uppercased() ?? ""
        //search for the right array using binary search
        let resultedArray = binarySearch(array: array, value: guidingLetter)
        //apply a regex expression to find the cities based on the search input
        let pattern = "\\b" + NSRegularExpression.escapedPattern(for: text.uppercased())
        return resultedArray.filter { $0.name.uppercased().range(of: pattern, options: [.anchored, .regularExpression]) != nil }
    }
}


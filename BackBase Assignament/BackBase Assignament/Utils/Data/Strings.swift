//
//  Strings.swift
//  BackBase Assignament
//
//  Created by Cojocaru Ionut Alexandru on 6/16/20.
//  Copyright © 2020 Cojocaru Ionut Alexandru. All rights reserved.
//

import Foundation

struct AppString {
    public static let citiesViewControllerTitle = "Cities"
    public static let latitudeLabel = "Latitude: "
    public static let longitudeLabel = "Longitude: "
    public static let searchBarText = "Search..."
    public static let tableViewNavBarRightButtonText = "Style"
    public static let mapViewControllerNavBarRightButtonText = "Style"
    public static let failedToRetrieveCitiesErrorTitle = "Failed to retrieve cities"
    public static let failedToRetrieveCitiesErrorMessage = "Please try again later"
}

struct CellIdentifiers {
    public static let cityCell = "cityCell"
}

//
//  CustomAlertNudgeView.swift
//  BackBase Assignament
//
//  Created by Cojocaru Ionut Alexandru on 6/18/20.
//  Copyright © 2020 Cojocaru Ionut Alexandru. All rights reserved.
//

import UIKit

class CustomAlertNudgeView: UIView {
    
    var components: AlertViewComponents? {
        didSet {
            guard let unwrappedComponents = components else { return }
            titleLabel.text = unwrappedComponents.title
            messageLabel.text = unwrappedComponents.message
            switch unwrappedComponents.type {
            case .error:
                icon.image = UIImage(named: "error_icon")
            }
        }
    }
    
    let icon: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.sizeToFit()
        
        return imageView
    }()
    
    let titleLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.boldSystemFont(ofSize: 18)
        label.textAlignment = .left
        label.textColor = .darkGray
        label.numberOfLines = 1
        
        return label
    }()
    
    let messageLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.systemFont(ofSize: 16)
        label.textAlignment = .left
        label.textColor = .gray
        label.numberOfLines = 1

        return label
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = .white
        setUpUI()
    }
    
    func setUpUI() {
        self.roundCorners()
        self.dropShadow()
        
        addSubview(icon)
        addSubview(titleLabel)
        addSubview(messageLabel)
        
        icon.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 10).isActive = true
        icon.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
        icon.heightAnchor.constraint(equalToConstant: 30).isActive = true
        icon.widthAnchor.constraint(equalToConstant: 30).isActive = true
        
        titleLabel.centerYAnchor.constraint(equalTo: icon.centerYAnchor, constant: -10).isActive = true
        titleLabel.leadingAnchor.constraint(equalTo: icon.trailingAnchor, constant: 10).isActive = true
        titleLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -10).isActive = true
        
        messageLabel.centerYAnchor.constraint(equalTo: icon.centerYAnchor, constant: 10).isActive = true
        messageLabel.leadingAnchor.constraint(equalTo: icon.trailingAnchor, constant: 15).isActive = true
        messageLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -10).isActive = true
    }
    
    public static func show(withMessage message: CustomAlertNudgeMessage) {
        guard let window = UIApplication.shared.windows.first else { return }

        let alertViewComponents = AlertViewComponents(title: message.title,
                                                      message: message.message,
                                                      type: message.type)
        let alertView = CustomAlertNudgeView()
        alertView.components = alertViewComponents
        alertView.translatesAutoresizingMaskIntoConstraints = false

        window.addSubview(alertView)
        
        let heightConstraint = alertView.topAnchor.constraint(equalTo: window.safeAreaLayoutGuide.topAnchor, constant: -200)
        heightConstraint.isActive = true
        
        alertView.leadingAnchor.constraint(equalTo: window.safeAreaLayoutGuide.leadingAnchor, constant: 10).isActive = true
        alertView.trailingAnchor.constraint(equalTo: window.trailingAnchor, constant: -10).isActive = true
        alertView.heightAnchor.constraint(equalToConstant: Constants.alertNudegeHeight).isActive = true
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            UIView.animate(withDuration: Constants.fastAnimationDuration, delay: 0, options: .curveEaseOut, animations: {
                  heightConstraint.constant = 10
                  window.layoutIfNeeded()
              }) { (_) in
                UIView.animate(withDuration: Constants.fastAnimationDuration, delay: 3, options: .curveEaseIn, animations: {
                        heightConstraint.constant = -200
                        window.layoutIfNeeded()
                    }) { (_) in
                        alertView.removeFromSuperview()
                    }
              }
        }
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

//
//  CustomErrorNudge.swift
//  BackBase Assignament
//
//  Created by Cojocaru Ionut Alexandru on 6/18/20.
//  Copyright © 2020 Cojocaru Ionut Alexandru. All rights reserved.
//

import UIKit

class CustomAlertNudgeMessage {
    var title: String
    var message: String
    var type: alertType

    init(title: String, message: String, type: alertType) {
        self.title = title
        self.message = message
        self.type = type
    }
}

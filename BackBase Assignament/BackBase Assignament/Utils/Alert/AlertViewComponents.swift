//
//  ErrorViewComponents.swift
//  BackBase Assignament
//
//  Created by Cojocaru Ionut Alexandru on 6/18/20.
//  Copyright © 2020 Cojocaru Ionut Alexandru. All rights reserved.
//

import Foundation

enum alertType: String {
    case error = "error"
}

struct AlertViewComponents {
    let title: String
    let message: String
    let type: alertType
}

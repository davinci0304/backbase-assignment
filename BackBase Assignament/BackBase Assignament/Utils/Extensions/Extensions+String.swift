//
//  Extensions+String.swift
//  BackBase Assignament
//
//  Created by Cojocaru Ionut Alexandru on 6/16/20.
//  Copyright © 2020 Cojocaru Ionut Alexandru. All rights reserved.
//

import UIKit

extension String {
    func concatenateBoldString(withRegularString string: String, withFontSize size: CGFloat = 15) -> NSAttributedString {
        let attributedString = NSMutableAttributedString(string: self,
                                                   attributes: [ NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: size)])
        attributedString.append(NSMutableAttributedString(string: string,
                                                    attributes: [NSAttributedString.Key.font: UIFont.systemFont(ofSize: size)]))
        
        return attributedString
    }
}

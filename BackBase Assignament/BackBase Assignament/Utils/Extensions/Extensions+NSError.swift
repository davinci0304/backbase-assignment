//
//  Extensions+NSError.swift
//  BackBase Assignament
//
//  Created by Cojocaru Ionut Alexandru on 6/22/20.
//  Copyright © 2020 Cojocaru Ionut Alexandru. All rights reserved.
//

import Foundation

extension NSError {
    static let fileError = NSError(domain: "fileError", code: 400, userInfo: nil)
}

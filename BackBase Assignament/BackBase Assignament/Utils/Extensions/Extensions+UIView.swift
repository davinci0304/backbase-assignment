//
//  Extensions+UIView.swift
//  BackBase Assignament
//
//  Created by Cojocaru Ionut Alexandru on 6/16/20.
//  Copyright © 2020 Cojocaru Ionut Alexandru. All rights reserved.
//

import UIKit

extension UIView {
    func dropShadow() {
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOpacity = 0.2
        self.layer.shadowOffset = CGSize.zero
        self.layer.shadowRadius = 3
        self.layer.shadowOffset = CGSize(width: 3, height: 3)
    }
    
    func roundCorners () {
//        self.clipsToBounds = true
        self.layer.cornerRadius = 10
        self.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMaxXMaxYCorner, .layerMinXMaxYCorner, .layerMinXMinYCorner]
    }
}

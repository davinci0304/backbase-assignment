//
//  Extensions+Colors.swift
//  BackBase Assignament
//
//  Created by Cojocaru Ionut Alexandru on 6/16/20.
//  Copyright © 2020 Cojocaru Ionut Alexandru. All rights reserved.
//

import UIKit

extension UIColor {
    static var lightPurple = UIColor(red: 210/255, green: 213/255, blue: 247/255, alpha: 1)
}

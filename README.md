# README #

1. After downloading and unzipping the app make sure you have Xcode installed on the machine
2. Go to assignment BB folder
3. Double click assignment BB.xcodeproj to open the project
4. After running the application you will be welcomed by a splash screen then redirect in the main screen where you will see a activity indicator while the data is preparing to be loaded into the table 
5. The  table has 2 viewing modes, a simple list and a grouped list and a search bar
6. By selecting a city you can navigate to a map view where the city is displayed by its coordinates
7. The map also have 2 viewing types, normal and satellite

Xcode preferred version 11.5
iOS preferred version 13.5
in order for the app to run you should change the bundle identifier